
capture program drop composcombos
program define composcombos 

syntax , n(integer) m(integer) [varnames(string)] [clear] [debug]

if "`clear'"!="" {
	clear
}
else if _N!=0 {
	di as error "dataset already loaded. Specify clear option"
	error 198
}

quietly {
local M = `m'
local N = `n'
local Mm1 = `M'-1
local Np1 = `N'+1

set obs `Np1'
tempvar expand filled
gen `expand' = 0
gen `filled' = 0

local m=1
while (`m' < `M') {
	expand `expand'
	if `m'==1 gen n`m'= _n-1
	else bys `sort': gen n`m'= _n-1
	replace `filled' = `filled'+n`m'
	replace `expand' = `Np1'-`filled'
	local sort `sort' n`m'
	local m=`m'+1
	if "`debug'"!="" list
}

gen n`M' = `N'-`filled'

if "`varnames'"!="" rename (n?) (`varnames')

compress
}

end



