
capture program drop datestring
program define datestring , rclass

syntax , ///
	[format(string)] // option to specify date format. "YMD" is assumed
	
if "`format'"=="" {
	local format YMD
}

if "`format'"!="MDY" & "`format'"!="MYD" & "`format'"!="YMD"  & "`format'"!="YDM" & "`format'"!="DMY" & "`format'"!="DYM" {
	di as error `"format "`format'" does not match one of YMD, YDM, MDY, MYD, DYM, or DMY"'
	error 198
}

// Date
local m=month(td($S_DATE))
if `m'<10 local m "0`m'"

local d=day(td($S_DATE))
if `d'<10 local d "0`d'"

local y=year(td($S_DATE))

if "`format'"=="YMD" local date "`y'`m'`d'"
if "`format'"=="YDM" local date "`y'`d'`m'"
if "`format'"=="DMY" local date "`d'`m'`y'"
if "`format'"=="DYM" local date "`d'`y'`m'"
if "`format'"=="MDY" local date "`m'`d'`y'"
if "`format'"=="MYD" local date "`m'`y'`d'"

// Time
tokenize $S_TIME , parse(":")
local time `1'`3'

// Date & Time
local date_time `date'_`1'`3'

return local time `time'
return local date `date'
return local date_time `date_time'

end
