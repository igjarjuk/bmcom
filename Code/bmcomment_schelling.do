


local griddim 10 // grid dimension
local n 10 // persons per neighborhood
local v 1 // number of additional vacancies per neighborhood
local pg1 0.5 // proportion of population in group 1
local ticks 10
local start 0.01
local samples 5 10 25 50 75

local seed 8642

local b_pg1_0 -1
local b_pg1_1 1
local b_orig 2
local b_dist -1
local b_vaca 1

tempfile history neighborhoods grid

// expand grid
clear
set obs `griddim'
gen int x = _n
expand `griddim'
bys x: gen int y = _n
save `grid', replace

expand `n'

// assign people to groups
set seed `seed'
gen byte g1 = rbinomial(1,`pg1')
gen byte t=0
// create an identifier
gen id = _n

compress
save `history'

collapse (mean) pg1=g1 (count) n=g1 , by(x y)
replace pg1=`pg1' if n==0

compress
save `neighborhoods'

qui forvalues t=1/`ticks' {
	
	use `history' if t==`t'-1, clear
	rename x xo
	rename y yo
	cross using `neighborhoods'

	gen dist = sqrt((x-xo)^2+(y-yo)^2)
	gen orig = xo==x & yo==y
	gen vaca = log(`n'+`v'-n + orig)
	replace vaca = 0 if vaca>=. & orig==1
	replace vaca = log(`start') if vaca>=.
	
	gen bx = 0
	replace bx = `b_pg1_0'*pg1 + `b_orig'*orig + `b_dist'*dist + `b_vaca'*vaca if g1==0
	replace bx = `b_pg1_1'*pg1 + `b_orig'*orig + `b_dist'*dist + `b_vaca'*vaca if g1==1
	bys id (x y): gen double p = sum(exp(bx))
	bys id (x y): replace p = exp(bx)/p[_N]
	
	bys id (x y): gen runif = runiform() if _n==1
	bys id (x y): replace runif = runif[1]
	bys id (x y): gen upper = sum(p)
	bys id (x y): gen lower = upper[_n-1]
	replace lower = 0 if lower>=.
	
	keep if runif>=lower & runif<upper
	keep id x y g1
	
	gen byte t = `t'
	append using `history'
	save `history', replace
	keep if t==`t'
	noisily di "individual summary"
	noisily sum g1
	
	collapse (mean) pg1=g1 (count) n=g1 , by(x y)
	merge 1:1 x y using `grid', nogen
	replace n=0 if n==.
	replace pg1=`pg1' if n==0
	compress
	save `neighborhoods', replace
	
	noisily di "neighborhood summary"
	noisily sum pg1 n

}

exit

use `history'
contract g1 x y t, zero freq(n)
reshape wide n , i(x y t) j(g1) 
gen int n = n0+n1
gen pg1 = n1/n
keep x y t pg1 n
replace pg1 = `pg1' if n==0
replace t = t+1
save `neighborhoods', replace


use `history'
bys id (t): gen int xo=x[_n-1]
bys id (t): gen int yo=y[_n-1]
rename (x y) (xd yd)
drop if t==0

joinby t using `neighborhoods'

gen byte choice = x==xd & y==yd
gen byte orig = x==xo & y==yo
gen byte mover = choice!=orig
gen dist = sqrt((x-xo)^2+(y-yo)^2)
gen long group = id*100 + t
gen vaca = log(`n'+`v'-n)
replace vaca = 0 if vaca>=. & orig
replace vaca = log(`start') if vaca>=.

gen top = -1*(choice | orig)

local J = `griddim'*`griddim'
gen u1 = runiform()
gen u2 = runiform()

qui foreach s of numlist `samples' `J' {
	noisily di "...`s'" _continue
	capture drop sample
	bysort id t (top u1 u2): gen byte sample = _n<=`s'
	capture drop c1
	gen c1 = -ln(1*(orig==1) + (`s'-1)/(`J'-1)*(orig==0))
	capture drop c2
	gen c2 = -ln( ///
		1*(orig==1 | choice==1) ///
		+ (`s'-1)/(`J'-1)*(orig==0 & choice==0 & mover==0) ///
		+ (`s'-2)/(`J'-2)*(orig==0 & choice==0 & mover==1) ///
		)
	if `s'==`J' {
		eststo g0 , noesample : clogit choice pg1 orig dist vaca if g1==0, group(group)
		eststo g1 , noesample : clogit choice pg1 orig dist vaca if g1==1, group(group)
	}
	if `s'!=`J' {
		eststo g0c1s`s' , noesample : clogit choice pg1 orig dist vaca if sample & g1==0, group(group) offset(c1)
		eststo g1c1s`s' , noesample : clogit choice pg1 orig dist vaca if sample & g1==1, group(group) offset(c1)
		eststo g0c2s`s' , noesample : clogit choice pg1 orig dist vaca if sample & g1==0, group(group) offset(c2)
		eststo g1c2s`s' , noesample : clogit choice pg1 orig dist vaca if sample & g1==1, group(group) offset(c2)
	}
} 

estimates table g0 g0c1*
estimates table g0 g0c2*

estimates table g1 g1c1*
estimates table g1 g1c2*



