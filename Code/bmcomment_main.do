/*
Paths
*/
global project /Users/bjarvis/Documents/Research/BMCOM
cd $project
/*
Programs
*/
qui do "$project/Code/program_composcombos.do"
qui do "$project/Code/program_datestring.do"
/*
Options
*/
set matsize 10000
set seed 2468
local estimate FALSE
local plot TRUE
local cssizes 5 10 20 50 100
local rgroup 1
local ivs asian black hispanic
local dv choice
local plotv b
local samples 200
local datadate 20171013
/***********************************
ESTIMATION STEP
***********************************/
if "`estimate'"=="TRUE" {
/*
Generate all possible compositions for ideal neighborhoods
*/
composcombos, n(14) m(4) varnames(asian black hispanic white) clear
tempfile idealcs
save `idealcs'
local J=_N
/*
Load the MCSUI data and clean up the respondent race variable
*/
use city caseid intvsex intvrace crace chispan gidlrac* gidlrc* eedudeg eeduyrs using "$project/Data/02535-0001-Data.dta" if city==3, clear
qui label dir
numlabel `r(names)', add 
label def crace 5 "5. latinx", add
replace crace = 5 if chispan==1
/*
Calculate the composition of the respondent's ideal neighborhood
*/
local rnames asian black hispanic white
local rcode = 1
gen idltotal = 0
foreach rname of local rnames {
	gen idl`rname' = 0
	foreach var of varlist gidlrac* gidlrc* {
		replace idl`rname'=idl`rname'+1 if `var'==`rcode'
	}
	local rcode=`rcode'+1
	replace idltotal = idltotal+idl`rname'
}
keep if idltotal==14 
drop gidlr*
compress
/*
Construct discrete choice data and outcome
*/
cross using `idealcs'
gen byte `dv' = idlasian==asian & idlblack==black & idlhispanic==hispanic & idlwhite==white 
gen byte neg`dv' = -1*`dv'
/*
Run on the full data and set up a matrix to store results
*/
clogit `dv' `ivs' if crace==`rgroup', group(caseid)
matrix estimates = 0,0,`J',e(N)/`J', e(N), e(b)
local colnames samplecase samplenum J N NJ `ivs'
matrix colnames estimates = `colnames' 
matrix coleq estimates = ""
/*
Loop through a bunch of samples and compile estimates
*/
tempvar r1 r2 c cs
forvalues i=1/`samples' {
	di as red "`i'" _continue
	capture drop `r1' `r2'
	/*
	generate choiceset sample
	*/
	gen `r1' = runiform()
	gen `r2' = runiform()
	sort caseid neg`dv' `r1' `r2'
	foreach cssize of numlist `cssizes' {
		di as text "." _continue
		capture drop `c' `cs'
		by caseid: gen byte `cs' = _n<=`cssize'
		/*
		calculate B-M sampling correction
		*/
		gen `c' = -1*ln((`cssize'-1)/(`J'-1))*(`dv'==0)
		/*
		estimate models with and without sampling correction
		*/	
		qui clogit `dv' `ivs' if crace==`rgroup' & `cs', group(caseid)
		matrix estimates = estimates \ 1 , `i', `cssize', e(N)/`cssize', e(N), e(b)
		qui clogit `dv' `ivs' if crace==`rgroup' & `cs', group(caseid) offset(`c')
		matrix estimates = estimates \ 2 , `i', `cssize', e(N)/`cssize', e(N), e(b)
	}
}
/*
save the matrix into data
*/
clear
svmat2 estimates , names(col)
label def samplecase 0 "no sampling" 1 "no correction" 2 "Bruch-Mare correction"
label val samplecase samplecase
/*
reshape the data for plotting and calculate bias
*/
rename (`ivs') b= 
reshape long b, i(samplecase samplenum J) j(race) string
bys race (samplecase): gen bfull = b[1]
gen bias = 100*(b-bfull)/bfull
label var bias "% Bias vs. Full Choice Set Model"
/*
save the results
*/
datestring
local today `r(date)'
save "$project/Data/bruchmare_comment_results", replace
}
/***********************************
PLOT STEP
***********************************/
if "`plot'"=="TRUE" {
if "`datadate'"=="today" local datadate `today'
use "$project/Data/bruchmare_comment_results", clear
/*
Clean up the data for plotting
*/
gen byte racecoef = .
replace racecoef = 1 if race=="asian"
replace racecoef = 2 if race=="black"
replace racecoef = 3 if race=="hispanic"
label def racecoef 1 "% Asian coef." 2 "% Black coef." 3 "% Hispanic coef."
label val racecoef
capture label drop samplecase
label def samplecase 0 "no sampling" 1 "no sampling correction" 2 "Bruch-Mare sampling correction"
label val samplecase samplecase
/* 
Plot summary of estimates
*/
set scheme s1mono
levelsof racecoef
local lvls `r(levels)'
foreach lvl of local lvls {
	if `lvl'==1 {
		local ylabel ylabel(, format(%8.1f))
		local fxsize fxsize(54)
	}
	else {
		local ylabel ylabel(, nolabels notick format(%8.1f))
		local fxsize fxsize(50)
	}
	/*
	Find the estimate for the full choice set and save for a yline
	*/
	qui sum `plotv' if samplecase==0 & racecoef==`lvl'
	local ref `r(mean)'
	/*
	Get the label to use for a subtitle
	*/	
	local title: label racecoef `lvl'
	#delim ;
	graph box `plotv' if samplecase!=0 & racecoef==`lvl'
		, over(samplecase) over(J) asy title("`title'")
		bar(1, lw(thin) color(gs1)) marker(1, msiz(*.5))
		bar(2, lw(thin) color(gs10)) marker(2, msiz(*.5))
		medtype(cline) medline(lw(thin))   
		nooutsides 
		`ylabel' `fxsize' ytitle("") 
		legend(region(lw(none))) name(`plotv'`lvl', replace) yline(`ref') nodraw note("") ;
	#delim cr
	local graphs `graphs' `plotv'`lvl'
}
#delim ;
grc1leg `graphs' ,
	 rows(1) imargin(zero) ring(100) ycommon 
	 b1title("choice set size") l1title("coefficient estimate") name(`plotv', replace) ;
#delim cr
graph export "$project/Figures/bruchmare_comment_results.pdf", name(`plotv') as(pdf) replace

}

